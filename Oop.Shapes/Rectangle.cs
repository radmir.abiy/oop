﻿using System;

namespace Oop.Shapes
{
    public class Rectangle : Shape
    {
        public Rectangle (int a, int b)
        {
            if (a <= 0)
            {
                throw new ArgumentOutOfRangeException("a");
            }
            if (b <= 0)
            {
                throw new ArgumentOutOfRangeException("b");
            }
            A = a;
            B = b;
        }

        public int A { get; }
        public int B { get; }

        public override double Area => A * B;

        public override double Perimeter => 2 * (A + B);

        public override int VertexCount => 4;

        public override bool IsEqual(Shape shape)
        {
            if (shape is Rectangle)
            {
                var rect = shape as Rectangle;
                return (this.A == rect.A && this.B == rect.B) ||
                       (this.B == rect.A && this.A == rect.B);
            }
            else
            {
                return false;
            }
        }
    }
}
