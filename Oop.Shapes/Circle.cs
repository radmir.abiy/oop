﻿using System;

namespace Oop.Shapes
{
    public class Circle : Shape
    {
		public Circle(int radius) 
        {
			if (radius <= 0)
            {
				throw new ArgumentOutOfRangeException("radius");
            }
			Radius = radius;
        }

		public int Radius { get; }

        public override double Area => Math.PI * Radius * Radius ;

        public override double Perimeter => 2 * Math.PI * Radius;

        public override int VertexCount => 0;

        public override bool IsEqual(Shape shape)
        {
			if (shape is Circle)
			{
				return this.Radius == (shape as Circle).Radius;
			}
			else
            {
				return false;
            }
        }
    }
}
