﻿using System;

namespace Oop.Shapes
{
    public class Triangle : Shape
    {

        public Triangle(int a, int b, int c) 
        {
            if (a <= 0)
            {
                throw new ArgumentOutOfRangeException("a");
            }
            if (b <= 0)
            {
                throw new ArgumentOutOfRangeException("b");
            }
            if (c <= 0)
            {
                throw new ArgumentOutOfRangeException("c");
            }
            if (a + b < c || a + c < b || b + c < a)
            {
                throw new InvalidOperationException() ;
            }
            A = a;
            B = b;
            C = c;
        }
        
        public int A { get; }
        public int B { get; }
        public int C { get; }

        public override double Area => Math.Sqrt((A + B + C) * (A - B + C) * (A + B - C) * (-A + B + C) / 16.0);

        public override double Perimeter => A + B + C;

        public override int VertexCount => 3;

        public override bool IsEqual(Shape shape)
        {
            if (shape is Triangle)
            {
                var shape2 = shape as Triangle;
                return (this.A == shape2.A && this.B == shape2.B && this.C == shape2.C) ||
                       (this.A == shape2.A && this.B == shape2.C && this.C == shape2.B) ||
                       (this.A == shape2.B && this.B == shape2.A && this.C == shape2.C) ||
                       (this.A == shape2.B && this.B == shape2.C && this.C == shape2.A) ||
                       (this.A == shape2.C && this.B == shape2.A && this.C == shape2.B) ||
                       (this.A == shape2.C && this.B == shape2.B && this.C == shape2.A);
            }
            else
            {
                return false;
            }
        }

    }
}
