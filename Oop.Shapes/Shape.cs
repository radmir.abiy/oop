﻿using System;
using System.Data;
using System.Security.Cryptography;

namespace Oop.Shapes
{
    public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public abstract double Area { get; }

		/// <summary>
		/// Периметр
		/// </summary>
		public abstract double Perimeter { get; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public abstract int VertexCount { get; }

		public abstract bool IsEqual(Shape shape);
	}
}
