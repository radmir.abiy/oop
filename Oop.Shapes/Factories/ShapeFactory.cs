﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			return new Circle(radius);
		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			return new Triangle(a, b, c);
		}

		public Shape CreateSquare(int a)
		{
			return new Square(a);
		}

		public Shape CreateRectangle(int a, int b)
		{
			return new Rectangle(a, b);
		}

	}
}