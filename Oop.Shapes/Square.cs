﻿using System;

namespace Oop.Shapes
{
    public class Square : Shape
    {
		public Square (int a)
        {
			if (a <= 0)
            {
				throw new ArgumentOutOfRangeException("a");
            }
			A = a;
        }

		public int A { get; }

        public override double Area => A * A;

        public override double Perimeter => 4 * A;

        public override int VertexCount => 4;

        public override bool IsEqual(Shape shape)
        {
            if (shape is Square)
            {
				return this.A == (shape as Square).A;
            }
			else
            {
				return false;
            }
        }
    }
}
